from cpt.packager import ConanMultiPackager

if __name__ == "__main__":
    builder = ConanMultiPackager()
    builder.add({"arch": "x86_64", "build_type": "Release"})
    builder.add({"arch": "x86_64", "build_type": "Debug"})
    # builder.add_common_builds()
    builder.run()
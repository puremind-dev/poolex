from conans import ConanFile, CMake, tools


class PoolExConan(ConanFile):
    name = "PoolExConan"
    version = "0.2"
    # license = "<Put the package license here>"
    # author = "<Vladimir> <puremind.dev@gmail.com>"
    # url = "<Package recipe repository url here, for issues about the package>"
    # description = "<Description of Conexample here>"
    # topics = ("<Put some tag here>", "<here>", "<and here>")
    settings = "os", "compiler", "build_type", "arch"
    # options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"gtest:shared": False, "gtest:build_gmock":True}
    generators = "cmake"
    requires = [
        "boost/1.71.0@conan/stable",
        "gtest/1.8.0@bincrafters/stable"
    ]

    def config_options(self):
        pass
        
    def config_options(self):
        pass

    def configure(self):
        pass

    def requirements(self):
        pass

    def package_id(self):
        pass

    def package_info(self):
        self.cpp_info.libs = ["poolex"]

    def deploy(self):
        pass


    def source(self):
        self.run("git clone https://gitlab.com/puremind-dev/poolex.git")
#         # This small hack might be useful to guarantee proper /MT /MD linkage
#         # in MSVC if the packaged project doesn't have variables to set it
#         # properly

#         tools.replace_in_file("conexample/CMakeLists.txt", "project(conexample VERSION 0.0.1)",
#                               '''project(conexample VERSION 0.0.1)

    def build(self):
        cmake = CMake(self)
        # cmake.configure(source_folder="../poolex_conan")
        cmake.configure()
        cmake.build()
        cmake.install()

    def package(self):
        self.copy("*.h", dst="include", src="src/include")
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["conexample"]


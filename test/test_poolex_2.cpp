#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <iostream>
#include <thread>
#include <chrono>

#include "poolex/poolex.h"

class PoolExFixture2 : public testing::Test{
public:
	PoolExFixture2()
	 :pool(4){

	}

	pool_ex_lib::PoolEx pool;
};


TEST_F(PoolExFixture2, fake){
	pool.run([](){
		std::cout << "hello1" << std::endl;
		// std::cerr << "hello2" << std::endl;
	});
	pool.run([](){
		std::cout << "hello2" << std::endl;
		// std::cerr << "hello2" << std::endl;
	});
	pool.run([](){
		std::cout << "hello3" << std::endl;
		// std::cerr << "hello2" << std::endl;
	});

	std::cout << "hello4" << std::endl;
	std::this_thread::sleep_for(std::chrono::seconds(3));

	ASSERT_EQ(1, 1);	
}


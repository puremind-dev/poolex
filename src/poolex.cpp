#include "poolex/poolex.h"
#include <iostream>

#include <boost/asio/thread_pool.hpp>
// #include <boost/asio/io_context.hpp>
// #include <boost/asio/any_io_executor.hpp>
// #include <boost/asio/executor_work_guard.hpp>
// #include <boost/asio/executor.hpp>
#include <boost/asio/post.hpp>
// #include <boost/asio/require.hpp>
// #include <boost/asio.hpp>

namespace pool_ex_lib {
	PoolEx::PoolEx(size_t numberOfThreads)
	: pool(std::make_unique<boost::asio::thread_pool>(numberOfThreads)){

	}

	// PoolEx::PoolEx(size_t numberOfThreads)
	// 	: io_context(std::make_shared<boost::asio::io_context>())
	// 	, work(std::make_unique<boost::asio::executor>((*io_context).get_executor()){

	// 	for(size_t num = 0; num != numberOfThreads; ++num){
	// 		threads.emplace_back([context = io_context, num](){
	// 			std::cout << "Thread number " << num << " started" << std::endl;

	// 			context->run();
	// 			std::cout << "Thread number " << num << " finished" << std::endl;
	// 		});
	// 	}		
	// }

	PoolEx::~PoolEx(){
		pool->join();
		// work.reset();
		// // io_context->stop();

		// for(auto & th : threads){
		// 	if(th.joinable())			
		// 		th.join();
		// }
	}

	void PoolEx::run(PoolLambda &&lambda){
		boost::asio::post(*pool, lambda);
	}
}
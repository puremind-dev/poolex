#ifndef POOL_EX_GUARD_H
#define POOL_EX_GUARD_H
#include <memory>
#include <thread>
#include <functional>
#include <vector>
// #include <boost/asio/io_context.hpp>

namespace boost{
	namespace asio{
		class io_context;
		// public:
		// 	class work;
		// };
		class executor;
		class thread_pool;
	  	
	}
}

namespace pool_ex_lib {
	using PoolLambda = std::function<void()>;

	class PoolEx final {
	public:
		explicit PoolEx(size_t numberOfThreads);
		~PoolEx();
		void run(PoolLambda &&lambda);

	private:
		std::unique_ptr<boost::asio::thread_pool> pool;
		// std::vector<std::thread> threads;
		// std::shared_ptr<boost::asio::io_context> io_context;
		// std::unique_ptr<boost::asio::executor_work_guard> work;		
	};
}

#endif